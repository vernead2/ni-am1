set -x

./grpcurl -plaintext localhost:8000 list
./grpcurl -plaintext localhost:8000 describe model.PaymentService

./grpcurl -plaintext localhost:8000 model.PaymentService.listPayments

read -r -d '' DATA <<- END
  {
    "cardNumber": "1234-1234-1234-1234", 
    "cardOwner": "CardOwner", 
    "orderIdentifier": "new order 5"
  }
END

./grpcurl -plaintext -d "$DATA" localhost:8000 model.PaymentService.addPayment

./grpcurl -plaintext localhost:8000 model.PaymentService.listPayments


# invalid card number
read -r -d '' DATA <<- END
  {
    "cardNumber": "aaaa-1234-1234-1234", 
    "cardOwner": "Card owner", 
    "orderIdentifier": "new order 5"
  }
END

./grpcurl -plaintext -d "$DATA" localhost:8000 model.PaymentService.addPayment

./grpcurl -plaintext localhost:8000 model.PaymentService.listPayments
