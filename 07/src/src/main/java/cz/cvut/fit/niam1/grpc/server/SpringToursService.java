package cz.cvut.fit.niam1.grpc.server;

import com.google.protobuf.BoolValue;
import com.google.protobuf.Empty;
import cz.cvut.fit.niam1.grpc.model.CardsProto.Card;
import cz.cvut.fit.niam1.grpc.model.CardsServiceGrpc.CardsServiceBlockingStub;
import cz.cvut.fit.niam1.grpc.model.PaymentProto.Payment;
import cz.cvut.fit.niam1.grpc.model.PaymentServiceGrpc.PaymentServiceImplBase;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.client.inject.GrpcClient;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@GrpcService
@SpringBootApplication
public class SpringToursService extends PaymentServiceImplBase {

    @Autowired
    private ServiceRepository repository;


    @GrpcClient("card")
    private CardsServiceBlockingStub cardServiceStub;

    public static void main(String[] args) {
        SpringApplication.run(SpringToursService.class, args);
    }

    @Override
    public void listPayments(
            Empty request, StreamObserver<Payment> responseObserver) {
        repository.getPayments().forEach(responseObserver::onNext);
        responseObserver.onCompleted();
    }

    @Override
    public void addPayment(Payment request, StreamObserver<BoolValue> responseObserver) {

        Card card = Card.newBuilder().setCardNumber(request.getCardNumber()).setCardOwner(request.getCardOwner()).build();

        BoolValue validated = cardServiceStub.validateCard(card);

        // validate card info
        if (!validated.getValue()) {
            responseObserver.onNext(BoolValue.newBuilder().setValue(false).build());
            responseObserver.onCompleted();
            return;
        }

        // add into repository
        if (!repository.addPayment(request)) {
            responseObserver.onNext(BoolValue.newBuilder().setValue(false).build());
            responseObserver.onCompleted();
            return;
        }

        responseObserver.onNext(BoolValue.newBuilder().setValue(true).build());
        responseObserver.onCompleted();

    }
}
