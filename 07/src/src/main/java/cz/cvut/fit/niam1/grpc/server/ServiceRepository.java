package cz.cvut.fit.niam1.grpc.server;

import cz.cvut.fit.niam1.grpc.model.PaymentProto.Payment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class ServiceRepository {

    private static final List<Payment> payments = new ArrayList<>();

    @PostConstruct
    public void initRepo() {
        this.addPayment(Payment.newBuilder().setCardNumber("1234-1234-1234-1234").setCardOwner("CardOwner").setOrderIdentifier("order-123").build());
        this.addPayment(Payment.newBuilder().setCardNumber("6666-6666-6666-6666").setCardOwner("Me Myself").setOrderIdentifier("order-420").build());
    }

    public boolean addPayment(Payment t) {
        Payment payment = payments.stream().filter(p -> p.getOrderIdentifier().equals(t.getOrderIdentifier())).findFirst().orElse(null);
        if (payment != null) {
            return false;
        }

        payments.add(t);
        return true;
    }

    public List<Payment> getPayments() {
        return payments;
    }

}
