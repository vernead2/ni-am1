from typing import Optional

from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter(
    prefix="/tour",
    tags=["tour"],
)


class Tour(BaseModel):
    name: str
    tour_id: str


class TourCreate(BaseModel):
    name: str


@router.get(
    "/",
    operation_id="list_tours",
    responses={
        200: {
            "description": "Read list of tours",
            "links": {
                "Self": {
                    "operationId": "list_tours",
                    "parameters": {
                        "country_id": "$request.query#/0/location_id",
                        "customer_id": "$request.query#/0/customer_id",
                    },
                },
                "read_tour": {
                    "operationId": "read_tour",
                    "parameters": {"tour_id": "$response.body#/0/tour_id"},
                },
                "create_tour": {
                    "operationId": "create_tour",
                },
            },
            "model": list[Tour],
        }
    },
)
def list_tours(
    country_id: Optional[int] = None, customer_id: Optional[int] = None
) -> list[Tour]:
    """Read list of tours"""
    raise NotImplementedError()


@router.post(
    "/",
    status_code=201,
    responses={
        201: {
            "description": "Create new tour",
            "links": {
                "Self": {
                    "operationId": "create_tour",
                },
                "read_tour": {
                    "operationId": "read_tour",
                    "parameters": {"tour_id": "$response.body#/tour_id"},
                },
                "list_tours": {
                    "operationId": "list_tours",
                },
            },
            "model": Tour,
        },
        400: {
            "description": "Tour with same name already exists",
            "links": {
                "Self": {
                    "operationId": "create_tour",
                },
            },
        },
    },
    operation_id="create_tour",
)
def create_tour(tour: TourCreate) -> Tour:
    raise NotImplementedError()


@router.get(
    "/{tour_id}",
    operation_id="read_tour",
    responses={
        200: {
            "description": "Create new tour",
            "links": {
                "Self": {
                    "operationId": "read_tour",
                    "parameters": {"tour_id": "$response.body#/tour_id"},
                },
                "update_tour": {
                    "operationId": "update_tour",
                    "parameters": {"tour_id": "$response.body#/tour_id"},
                },
                "delete_tour": {
                    "operationId": "delete_tour",
                    "parameters": {"tour_id": "$response.body#/tour_id"},
                },
            },
            "model": Tour,
        },
        404: {
            "description": "invalid tour_id",
            "links": {
                "list_tours": {
                    "operationId": "list_tours",
                }
            },
        },
    },
)
def read_tour(tour_id: int) -> Tour:
    raise NotImplementedError()


@router.patch(
    "/{tour_id}",
    operation_id="update_tour",
    responses={
        200: {
            "description": "Create new tour",
            "links": {
                "Self": {
                    "operationId": "update_tour",
                    "parameters": {"tour_id": "$response.body#/tour_id"},
                },
                "read_tour": {
                    "operationId": "read_tour",
                    "parameters": {"tour_id": "$response.body#/tour_id"},
                },
                "delete_tour": {
                    "operationId": "delete_tour",
                    "parameters": {"tour_id": "$response.body#/tour_id"},
                },
            },
            "model": Tour,
        },
        404: {
            "description": "invalid tour_id",
            "links": {
                "list_tours": {
                    "operationId": "list_tours",
                }
            },
        },
    },
)
def update_tour(tour_id: int, tour: TourCreate) -> Tour:
    raise NotImplementedError()


@router.delete(
    "/{tour_id}",
    status_code=204,
    responses={
        204: {
            "description": "Create new tour",
            "links": {
                "list_tours": {
                    "operationId": "list_tours",
                }
            },
        },
        404: {
            "description": "invalid tour_id",
            "links": {
                "list_tours": {
                    "operationId": "list_tours",
                }
            },
        },
    },
)
def delete_tour(tour_id: int):
    raise NotImplementedError()
