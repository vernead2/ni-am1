from typing import Optional
from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter(
    prefix="/location",
    tags=["location"],
)


class Location(BaseModel):
    name: str
    location_id: str
    country_id: str


class LocationCreate(BaseModel):
    name: str
    country_id: str


@router.get(
    "/",
    operation_id="list_locations",
    responses={
        200: {
            "description": "Read list of locations",
            "links": {
                "Self": {
                    "operationId": "list_locations",
                    "parameters": {"country_id": "$request.query#/0/location_id"},
                },
                "read_location": {
                    "operationId": "read_location",
                    "parameters": {"location_id": "$response.body#/0/location_id"},
                },
                "create_location": {
                    "operationId": "create_location",
                },
            },
            "model": list[Location],
        }
    },
)
def list_locations(country_id: Optional[int] = None) -> list[Location]:
    """Read list of locations"""
    raise NotImplementedError()


@router.post(
    "/",
    status_code=201,
    responses={
        201: {
            "description": "Create new location",
            "links": {
                "Self": {
                    "operationId": "create_location",
                },
                "read_location": {
                    "operationId": "read_location",
                    "parameters": {"location_id": "$response.body#/location_id"},
                },
                "list_locations": {
                    "operationId": "list_locations",
                },
            },
            "model": Location,
        },
        400: {
            "description": "Location with same name already exists",
            "links": {
                "Self": {
                    "operationId": "create_location",
                },
            },
        },
    },
    operation_id="create_location",
)
def create_location(location: LocationCreate) -> Location:
    raise NotImplementedError()


@router.get(
    "/{location_id}",
    operation_id="read_location",
    responses={
        200: {
            "description": "Create new location",
            "links": {
                "Self": {
                    "operationId": "read_location",
                    "parameters": {"location_id": "$response.body#/location_id"},
                },
                "update_location": {
                    "operationId": "update_location",
                    "parameters": {"location_id": "$response.body#/location_id"},
                },
                "delete_location": {
                    "operationId": "delete_location",
                    "parameters": {"location_id": "$response.body#/location_id"},
                },
                "list_tours": {
                    "operationId": "list_tours",
                    "parameters": {"location_id": "$response.body#/location_id"},
                },
            },
            "model": Location,
        },
        404: {
            "description": "invalid location_id",
            "links": {
                "list_locations": {
                    "operationId": "list_locations",
                }
            },
        },
    },
)
def read_location(location_id: int) -> Location:
    raise NotImplementedError()


@router.patch(
    "/{location_id}",
    operation_id="update_location",
    responses={
        200: {
            "description": "Create new location",
            "links": {
                "Self": {
                    "operationId": "update_location",
                    "parameters": {"location_id": "$response.body#/location_id"},
                },
                "read_location": {
                    "operationId": "read_location",
                    "parameters": {"location_id": "$response.body#/location_id"},
                },
                "delete_location": {
                    "operationId": "delete_location",
                    "parameters": {"location_id": "$response.body#/location_id"},
                },
            },
            "model": Location,
        },
        404: {
            "description": "invalid location_id or country_id",
            "links": {
                "list_locations": {
                    "operationId": "list_locations",
                }
            },
        },
    },
)
def update_location(location_id: int, location: LocationCreate) -> Location:
    raise NotImplementedError()


@router.delete(
    "/{location_id}",
    status_code=204,
    responses={
        204: {
            "description": "Create new location",
            "links": {
                "list_locations": {
                    "operationId": "list_locations",
                }
            },
        },
        404: {
            "description": "invalid location_id",
            "links": {
                "list_locations": {
                    "operationId": "list_locations",
                }
            },
        },
    },
)
def delete_location(location_id: int):
    raise NotImplementedError()
