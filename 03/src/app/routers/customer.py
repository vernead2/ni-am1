from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter(
    prefix="/customer",
    tags=["customer"],
)


class Customer(BaseModel):
    name: str
    customer_id: str


class CustomerCreate(BaseModel):
    name: str


@router.get(
    "/",
    operation_id="list_customers",
    responses={
        200: {
            "description": "Read list of customers",
            "links": {
                "Self": {
                    "operationId": "list_customers",
                },
                "read_customer": {
                    "operationId": "read_customer",
                    "parameters": {"customer_id": "$response.body#/0/customer_id"},
                },
                "CreateCustomer": {
                    "operationId": "create_customer",
                },
            },
            "model": list[Customer],
        }
    },
)
def list_customers() -> list[Customer]:
    """Read list of customers"""
    raise NotImplementedError()


@router.post(
    "/",
    status_code=201,
    responses={
        201: {
            "description": "Create new customer",
            "links": {
                "Self": {
                    "operationId": "create_customer",
                },
                "read_customer": {
                    "operationId": "read_customer",
                    "parameters": {"customer_id": "$response.body#/customer_id"},
                },
                "list_customers": {
                    "operationId": "list_customers",
                },
            },
            "model": Customer,
        },
        400: {
            "description": "Customer with same name already exists",
            "links": {
                "Self": {
                    "operationId": "create_customer",
                },
            },
        },
    },
    operation_id="create_customer",
)
def create_customer(customer: CustomerCreate) -> Customer:
    raise NotImplementedError()


@router.get(
    "/{customer_id}",
    operation_id="read_customer",
    responses={
        200: {
            "description": "Create new customer",
            "links": {
                "Self": {
                    "operationId": "read_customer",
                    "parameters": {"customer_id": "$response.body#/customer_id"},
                },
                "update_customer": {
                    "operationId": "update_customer",
                    "parameters": {"customer_id": "$response.body#/customer_id"},
                },
                "delete_customer": {
                    "operationId": "delete_customer",
                    "parameters": {"customer_id": "$response.body#/customer_id"},
                },
                "list_tours": {
                    "operationId": "list_tours",
                    "parameters": {"customer_id": "$response.body#/customer_id"},
                },
            },
            "model": Customer,
        },
        404: {
            "description": "invalid customer_id",
            "links": {
                "list_customers": {
                    "operationId": "list_customers",
                }
            },
        },
    },
)
def read_customer(customer_id: int) -> Customer:
    raise NotImplementedError()


@router.patch(
    "/{customer_id}",
    operation_id="update_customer",
    responses={
        200: {
            "description": "Create new customer",
            "links": {
                "Self": {
                    "operationId": "update_customer",
                    "parameters": {"customer_id": "$response.body#/customer_id"},
                },
                "read_customer": {
                    "operationId": "read_customer",
                    "parameters": {"customer_id": "$response.body#/customer_id"},
                },
                "delete_customer": {
                    "operationId": "delete_customer",
                    "parameters": {"customer_id": "$response.body#/customer_id"},
                },
            },
            "model": Customer,
        },
        404: {
            "description": "invalid customer_id",
            "links": {
                "list_customers": {
                    "operationId": "list_customers",
                }
            },
        },
    },
)
def update_customer(customer_id: int, customer: CustomerCreate) -> Customer:
    raise NotImplementedError()


@router.delete(
    "/{customer_id}",
    status_code=204,
    responses={
        204: {
            "description": "Create new customer",
            "links": {
                "list_customers": {
                    "operationId": "list_customers",
                }
            },
        },
        404: {
            "description": "invalid customer_id",
            "links": {
                "list_customers": {
                    "operationId": "list_customers",
                }
            },
        },
    },
)
def delete_customer(customer_id: int):
    raise NotImplementedError()
