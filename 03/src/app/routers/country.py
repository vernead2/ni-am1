from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter(
    prefix="/country",
    tags=["country"],
)


class Country(BaseModel):
    name: str
    country_id: str


class CountryCreate(BaseModel):
    name: str


@router.get(
    "/",
    operation_id="list_countries",
    responses={
        200: {
            "description": "Read list of countries",
            "links": {
                "Self": {
                    "operationId": "list_countries",
                },
                "read_country": {
                    "operationId": "read_country",
                    "parameters": {"country_id": "$response.body#/0/country_id"},
                },
                "create_country": {
                    "operationId": "create_country",
                },
            },
            "model": list[Country],
        }
    },
)
def list_countries() -> list[Country]:
    """Read list of countries"""
    raise NotImplementedError()


@router.post(
    "/",
    status_code=201,
    responses={
        201: {
            "description": "Create new country",
            "links": {
                "Self": {
                    "operationId": "create_country",
                },
                "read_country": {
                    "operationId": "read_country",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
                "list_countries": {
                    "operationId": "list_countries",
                },
                "create_location": {
                    "operationId": "create_location",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
            },
            "model": Country,
        },
        400: {
            "description": "Country with same name already exists",
            "links": {
                "Self": {
                    "operationId": "create_country",
                },
            },
        },
    },
    operation_id="create_country",
)
def create_country(country: CountryCreate) -> Country:
    raise NotImplementedError()


@router.get(
    "/{country_id}",
    operation_id="read_country",
    responses={
        200: {
            "description": "Create new country",
            "links": {
                "Self": {
                    "operationId": "read_country",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
                "update_country": {
                    "operationId": "update_country",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
                "delete_country": {
                    "operationId": "delete_country",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
                "list_locations": {
                    "operationId": "list_locations",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
                "create_location": {
                    "operationId": "create_location",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
            },
            "model": Country,
        },
        404: {
            "description": "invalid country_id",
            "links": {
                "list_countries": {
                    "operationId": "list_countries",
                }
            },
        },
    },
)
def read_country(country_id: int) -> Country:
    raise NotImplementedError()


@router.patch(
    "/{country_id}",
    operation_id="update_country",
    responses={
        200: {
            "description": "Create new country",
            "links": {
                "Self": {
                    "operationId": "update_country",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
                "read_country": {
                    "operationId": "read_country",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
                "delete_country": {
                    "operationId": "delete_country",
                    "parameters": {"country_id": "$response.body#/country_id"},
                },
            },
            "model": Country,
        },
        404: {
            "description": "invalid country_id",
            "links": {
                "list_countries": {
                    "operationId": "list_countries",
                }
            },
        },
    },
)
def update_country(country_id: int, country: CountryCreate) -> Country:
    raise NotImplementedError()


@router.delete(
    "/{country_id}",
    status_code=204,
    responses={
        204: {
            "description": "Create new country",
            "links": {
                "list_countries": {
                    "operationId": "list_countries",
                }
            },
        },
        404: {
            "description": "invalid country_id",
            "links": {
                "list_countries": {
                    "operationId": "list_countries",
                }
            },
        },
    },
)
def delete_country(country_id: int):
    raise NotImplementedError()
