from typing import Union

from fastapi import FastAPI
from pydantic import BaseModel

from .routers import country
from .routers import customer
from .routers import location
from .routers import tour

app = FastAPI(
    title="Travel Agency API",
    version="0.0.1",
    description="RESTful API Design for Travel Agency",
    servers=[{"url": "http://t-a.org"}],
)

app.include_router(country.router)
app.include_router(customer.router)
app.include_router(location.router)
app.include_router(tour.router)
