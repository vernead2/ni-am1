package cz.cvut.fit.niam1;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.commons.text.StringEscapeUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class JsonMatcher extends BaseMatcher<String> {
    private final String expected;

    public JsonMatcher(String expected) {
        this.expected = expected;
    }

    @SneakyThrows
    @Override
    public boolean matches(Object o) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree((String) o).equals(mapper.readTree(expected));
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("Compared objects differ\n");
        description.appendText("Expected: " + StringEscapeUtils.escapeJava(expected));
    }

}

@SpringBootTest
@AutoConfigureMockMvc
class Hw01ApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testTransform() {
        // load message from file input01.example
        String message = """
                Dear Sir or Madam,
                please find the details about my booking below:
                ===
                Tour id: "1"
                Location: "Bohemian Switzerland"
                Person: "Jan Novak"
                ===
                Regards,
                Jan Novak""";

        BookingInformation bookingInformation = new BookingInformation(message);
        System.out.println(bookingInformation);
    }

    @Test
    void testExamples() throws Exception {

        String[][] examples = {
                {"examples/input01.example", "examples/output01.example", "success"},
                {"examples/input02.example", "examples/output02.example", "failure"},
                {"examples/input03.example", "examples/output03.example", "failure"},
                {"examples/input04.example", "examples/output04.example", "failure"},
                {"examples/input05.example", "examples/output05.example", "failure"},
        };

        // iterate through examples
        for (String[] example : examples) {

            String input = new String(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(example[0])).readAllBytes(), StandardCharsets.UTF_8);
            String output = new String(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(example[1])).readAllBytes(), StandardCharsets.UTF_8);

            this.mockMvc.perform(
                            post("/transform", input)
                                    .contentType(MediaType.TEXT_PLAIN)
                                    .content(input)
                                    .accept(MediaType.APPLICATION_JSON)
                    )
                    //.andDo(print())
                    .andExpect((example[2].equals("success") ? status().isOk(): status().isBadRequest()))
                    .andExpect(content().string(new JsonMatcher(output)));

        }
    }

}
