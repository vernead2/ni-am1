package cz.cvut.fit.niam1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@RestController
@SpringBootApplication
public class Hw01Application {

    @PostMapping(value = "/transform", consumes= "text/plain", produces = "application/json")
    @ResponseBody
    BookingInformation transformation(@RequestBody String message) {
        return new BookingInformation(message);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(org.springframework.http.HttpStatus.BAD_REQUEST)
    @ResponseBody
    String handleIllegalArgumentException(IllegalArgumentException e) {
        return "{\"error\": \"" + e.getMessage() + "\"}";
    }

    public static void main(String[] args) {
        SpringApplication.run(Hw01Application.class, args);
    }

}
