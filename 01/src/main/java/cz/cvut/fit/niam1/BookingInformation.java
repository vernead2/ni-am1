package cz.cvut.fit.niam1;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class BookingInformation {

    private record Person(@JsonView String name, @JsonView String surname) {
        private Person(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }
    }

    @JsonView
    private final String id;
    @JsonView
    private final String location;
    @JsonView
    private final Person person;

    private static final Pattern DATA_PATTERN = Pattern.compile("===\\s*([^=]*)\\s*===", Pattern.MULTILINE);
    private static final Pattern ID_PATTERN = Pattern.compile("id:\\s*\"(.*)\"", Pattern.MULTILINE);
    private static final Pattern LOCATION_PATTERN = Pattern.compile("Location:\\s*\"([^\"]*)\"", Pattern.MULTILINE);
    private static final Pattern NAME_PATTERN = Pattern.compile("Person:\\s*\"(\\w*)\\s*(\\w*)\"", Pattern.MULTILINE);


    public BookingInformation(String message) throws IllegalArgumentException {

        // match data section in message
        Matcher matcher = DATA_PATTERN.matcher(message);
        if (!matcher.find())
            throw new IllegalArgumentException("Invalid message format, no data section found");

        String booking_data = matcher.group(1);

        Matcher match_name = NAME_PATTERN.matcher(booking_data);
        if (!match_name.find())
            throw new IllegalArgumentException("Invalid message format, no person found");

        Matcher match_id = ID_PATTERN.matcher(booking_data);
        if (!match_id.find())
            throw new IllegalArgumentException("Invalid message format, no id found");

        Matcher match_location = LOCATION_PATTERN.matcher(booking_data);
        if (!match_location.find())
            throw new IllegalArgumentException("Invalid message format, no location found");


        id = match_id.group(1);
        location = match_location.group(1);
        person = new Person(match_name.group(1), match_name.group(2));
    }

}


