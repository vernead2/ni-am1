package cz.cvut.fit.niam1.async_rest;

public class Tour {
    private final String id;
    private final String name;
    private boolean awaiting_delete = false;

    Tour(String id, String name) {
        this.id = id;
        this.name = name;
        awaiting_delete=false;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean getAwaitingDelete() {
        return awaiting_delete;
    }

    public void setAwaitingDelete(boolean awaiting_delete) {
        this.awaiting_delete = awaiting_delete;
    }

}
