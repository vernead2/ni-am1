package cz.cvut.fit.niam1.async_rest;


import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class AsyncRestApplicationRepository {

    private static List<Tour> tours = new ArrayList<>();

    @PostConstruct
    public void initRepo() {
        tours.add(new Tour("1", "Beach tour"));
        tours.add(new Tour("2", "Ski tour"));
        tours.add(new Tour("3", "City tour"));
    }

    public List<Tour> getTours(boolean include_deleted) {
        if (!include_deleted) {
            return tours.stream().filter(c -> !c.getAwaitingDelete()).toList();
        }
        return tours;
    }

    public Tour getTourById(String id) {
        return tours.stream().filter(c -> c.getId().equals(id)).findAny().orElse(null);
    }

    public void deleteTour(String id) {
        Tour tour = getTourById(id);
        if (tour == null) {
            throw new IllegalArgumentException("Tour with id " + id + " does not exist");
        }

        if (tour.getAwaitingDelete())
            throw new IllegalArgumentException("Tour with id " + id + " is already awaiting delete");

        tour.setAwaitingDelete(true);

        // spawn another thread which will delete the tour after 10 seconds
        new Thread(() -> {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("Deleting tour " + id);
            tours.removeIf(c -> c.getId().equals(id) && c.getAwaitingDelete());
        }).start();
    }

}
