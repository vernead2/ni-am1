package cz.cvut.fit.niam1.async_rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class AsyncRestApplicationController {

    @Autowired
    AsyncRestApplicationRepository repository;

    @GetMapping("/tours")
    @ResponseStatus(HttpStatus.OK)
    public List<Tour> getTours(@RequestParam(required = false, defaultValue = "false") boolean include_deleted) {
        return repository.getTours(include_deleted);
    }

    @DeleteMapping("/tour/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTour(@PathVariable String id) {
        repository.deleteTour(id);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(org.springframework.http.HttpStatus.BAD_REQUEST)
    @ResponseBody
    String handleIllegalArgumentException(IllegalArgumentException e) {
        return "{\"error\": \"" + e.getMessage() + "\"}";
    }
}
