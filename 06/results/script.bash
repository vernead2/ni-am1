set -x

./grpcurl -plaintext localhost:8000 list
./grpcurl -plaintext localhost:8000 describe model.ToursService

./grpcurl -plaintext localhost:8000 model.ToursService.getTours

read -r -d '' DATA <<- END
  {
    "name": "test tour",
    "passengers": ["passenger 1", "passenger 1"],
    "departure": {
      "date": "2023-11-22T11:08:12.000Z",
      "identifier": "PRG"
    },
    "arrival": {
      "date": "2023-11-22T11:28:52.000Z",
      "identifier": "VIE"
    }
  }
END

./grpcurl -plaintext -d "$DATA" localhost:8000 model.ToursService.addTour

./grpcurl -plaintext localhost:8000 model.ToursService.getTours

# update tour
./grpcurl -plaintext -d '{"id": 1, "name": "updated tour"}' localhost:8000 model.ToursService.updateTour

./grpcurl -plaintext localhost:8000 model.ToursService.getTours

# delete tours
./grpcurl -plaintext -d '{"id": 1}' localhost:8000 model.ToursService.deleteTour

./grpcurl -plaintext localhost:8000 model.ToursService.getTours
