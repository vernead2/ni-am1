package cz.cvut.fit.niam1.grpc.server;

import com.google.protobuf.Empty;
import cz.cvut.fit.niam1.grpc.model.*;
import io.grpc.internal.testing.StreamRecorder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Configuration
class MyServiceUnitTestConfiguration {

    @Bean
    SpringToursService myService() {
        return new SpringToursService();
    }

    @Bean
    ServiceRepository repository() {
        return new ServiceRepository();
    }

}

@SpringBootTest
@SpringJUnitConfig(classes = {MyServiceUnitTestConfiguration.class})
class SpringToursServiceTest {

    @Autowired
    private SpringToursService myService;

    @Test
    void testGetTours() throws Exception {
        Empty request = Empty.newBuilder()
                .build();
        StreamRecorder<Tour> responseObserver = StreamRecorder.create();
        myService.getTours(request, responseObserver);

        if (!responseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            Assertions.fail("The call did not terminate in time");
        }
        Assertions.assertNull(responseObserver.getError());
        List<Tour> results = responseObserver.getValues();
        Assertions.assertEquals(3, results.size());
        Tour response = results.get(0);

Assertions.assertEquals(1, response.getId());
        Assertions.assertEquals("Oslo Tour", response.getName());
        Assertions.assertEquals("Karel", response.getPassengers(0));
        Assertions.assertEquals("David", response.getPassengers(1));
        Assertions.assertEquals("PRG", response.getDeparture().getIdentifier());
        Assertions.assertEquals("OSL", response.getArrival().getIdentifier());
    }

    @Test
    void testCreateTour() throws Exception{
        TourAdd request = TourAdd.newBuilder()
                .setName("Berlin Tour")
                .addPassengers("Karel")
                .addPassengers("David")
                .setDeparture(
                        AirportInfo.newBuilder().setIdentifier("PRG").build()
                )
                .setArrival(
                        AirportInfo.newBuilder().setIdentifier("TXL").build()
                )
                .build();
        StreamRecorder<Tour> responseObserver = StreamRecorder.create();
        myService.addTour(request, responseObserver);

        if (!responseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            Assertions.fail("The call did not terminate in time");
        }
        Assertions.assertNull(responseObserver.getError());
        Tour response = responseObserver.getValues().get(0);

        Assertions.assertEquals(4, response.getId());
        Assertions.assertEquals("Berlin Tour", response.getName());
        Assertions.assertEquals("Karel", response.getPassengers(0));
        Assertions.assertEquals("David", response.getPassengers(1));
        Assertions.assertEquals("PRG", response.getDeparture().getIdentifier());
        Assertions.assertEquals("TXL", response.getArrival().getIdentifier());
    }

    @Test
    void testUpdateTour() throws Exception {
        TourUpdate request = TourUpdate.newBuilder()
                .setId(1)
                .setName("Helsinki Tour")
                .addRemovedPassengers("Karel")
                .addNewPassengers("Ondrej")
                .setArrival(
                        AirportInfo.newBuilder().setIdentifier("HEL").build()
                )
                .build();

        StreamRecorder<Tour> responseObserver = StreamRecorder.create();
        myService.updateTour(request, responseObserver);

        if (!responseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            Assertions.fail("The call did not terminate in time");
        }
        Assertions.assertNull(responseObserver.getError());
        Tour response = responseObserver.getValues().get(0);

        Assertions.assertEquals(1, response.getId());
        Assertions.assertEquals("Helsinki Tour", response.getName());
        Assertions.assertEquals("David", response.getPassengers(0));
        Assertions.assertEquals("Ondrej", response.getPassengers(1));
        Assertions.assertEquals("PRG", response.getDeparture().getIdentifier());
        Assertions.assertEquals("HEL", response.getArrival().getIdentifier());
    }

    @Test
    void testDeleteTour() throws Exception {
        TourDelete request = TourDelete.newBuilder()
                .setId(1)
                .build();

        StreamRecorder<Empty> responseObserver = StreamRecorder.create();
        myService.deleteTour(request, responseObserver);

        if (!responseObserver.awaitCompletion(5, TimeUnit.SECONDS)) {
            Assertions.fail("The call did not terminate in time");
        }
        Assertions.assertNull(responseObserver.getError());
        Empty response = responseObserver.getValues().get(0);
    }

}
