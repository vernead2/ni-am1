package cz.cvut.fit.niam1.grpc.server;

import com.google.protobuf.ProtocolStringList;
import cz.cvut.fit.niam1.grpc.model.AirportInfo;
import cz.cvut.fit.niam1.grpc.model.Tour;
import cz.cvut.fit.niam1.grpc.model.TourAdd;
import cz.cvut.fit.niam1.grpc.model.TourUpdate;
import cz.cvut.fit.niam1.grpc.model.TourDelete;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class ServiceRepository {

    private static final List<Tour> tours = new ArrayList<>();
    private static int next_id = 1;

    @PostConstruct
    public void initRepo() {
        tours.add(
                Tour.newBuilder().setId(1).setName("Oslo Tour").addPassengers("Karel").addPassengers("David").setDeparture(
                        AirportInfo.newBuilder().setIdentifier("PRG").build()
                ).setArrival(
                        AirportInfo.newBuilder().setIdentifier("OSL").build()
                ).build()
        );
        tours.add(
                Tour.newBuilder().setId(2).setName("Paris Tour").addPassengers("Josef").setDeparture(
                        AirportInfo.newBuilder().setIdentifier("PRG").build()
                ).setArrival(
                        AirportInfo.newBuilder().setIdentifier("CDG").build()
                ).build()
        );
        tours.add(
                Tour.newBuilder().setId(3).setName("London Tour").addPassengers("Bobby").setDeparture(
                        AirportInfo.newBuilder().setIdentifier("PRG").build()
                ).setArrival(
                        AirportInfo.newBuilder().setIdentifier("LHR").build()
                ).build()
        );
        next_id = 4;
    }

    public Tour addTour(TourAdd t) {
        Tour new_tour = Tour
                .newBuilder()
                .setId(next_id++)
                .setName(t.getName())
                .setDeparture(t.getDeparture())
                .setArrival(t.getArrival())
                .addAllPassengers(t.getPassengersList())
                .build();

        tours.add(new_tour);
        return new_tour;
    }

    public void removeTour(TourDelete t) {
        tours.removeIf(tour -> tour.getId() == t.getId());
    }

    public Tour getTourById(long id) {
        return tours.stream().filter(t -> t.getId() == id).findFirst().orElse(null);
    }

    public List<Tour> getTours() {
        return tours;
    }

    public Tour updateTour(TourUpdate update_info) {
        Tour tour = getTourById(update_info.getId());
        if (tour == null)
            return null;

        Tour.Builder builder = Tour.newBuilder().setId(tour.getId());

        builder.setName(update_info.hasName() ? update_info.getName() : tour.getName());
        builder.setDeparture(update_info.hasDeparture() ? update_info.getDeparture() : tour.getDeparture());
        builder.setArrival(update_info.hasArrival() ? update_info.getArrival() : tour.getArrival());

        // remove passengers from update_info.removed_passengers
        List<String> passenger_list = new ArrayList<>(tour.getPassengersList());
        ProtocolStringList removed_passengers = update_info.getRemovedPassengersList();
        for (String passenger : removed_passengers) {
            passenger_list.remove(passenger);
        }
        // add new passengers
        ProtocolStringList new_passengers = update_info.getNewPassengersList();
        passenger_list.addAll(new_passengers);

        builder.addAllPassengers(passenger_list);

        Tour new_tour = builder.build();

        // replace tour in original list
        tours.replaceAll(t -> t.getId() == tour.getId() ? new_tour : t);

        return new_tour;
    }

    public void deleteTour(String name) {
        tours.removeIf(t -> t.getName().equals(name));
    }


}
