package cz.cvut.fit.niam1.grpc.server;

import com.google.protobuf.Empty;
import cz.cvut.fit.niam1.grpc.model.Tour;
import cz.cvut.fit.niam1.grpc.model.TourAdd;
import cz.cvut.fit.niam1.grpc.model.TourDelete;
import cz.cvut.fit.niam1.grpc.model.TourUpdate;
import cz.cvut.fit.niam1.grpc.model.ToursServiceGrpc.ToursServiceImplBase;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@GrpcService
@SpringBootApplication
public class SpringToursService extends ToursServiceImplBase {

    @Autowired
    private ServiceRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(SpringToursService.class, args);
    }

    @Override
    public void getTours(Empty request, StreamObserver<Tour> responseObserver) {

        repository.getTours().forEach(responseObserver::onNext);
        responseObserver.onCompleted();
    }

    @Override
    public void addTour(TourAdd request, StreamObserver<Tour> responseObserver) {
        responseObserver.onNext(repository.addTour(request));
        responseObserver.onCompleted();
    }

    @Override
  public void deleteTour(TourDelete request, StreamObserver<Empty> responseObserver) {
    repository.removeTour(request);
    responseObserver.onNext(Empty.getDefaultInstance());
    responseObserver.onCompleted();
  }

  @Override
  public void updateTour(TourUpdate request, StreamObserver<Tour> responseObserver) {
    responseObserver.onNext(repository.updateTour(request));
    responseObserver.onCompleted();
  }
}
