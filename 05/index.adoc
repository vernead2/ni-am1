= HW5 - TBD!

Vytvořil jsem API se 4 endpointy

 - `POST /tour/{id}/customer` request body se uloží jako customer k příslušnému výletu. Akutalizuje lastModified proměnnou v repozitáři.
 - `GET /tours/last-modified` Provádí cachování na základě `If-Modified-Since` headeru.
 - `GET /tours/weak-tag` Generuje (a kontroluje) etag na základě parametru id a name v objektech. Seznam zákazníků ignoruje.
 - `GET /tours/strong-tag` Generovaný ETag odpovídá MD5 digestu celé zprávy, tímpádem se dotaz znovu vrací v případě změny zákazníků.


Záznamy terminálu, kde jsou testovaný jednotlivé přístup jsou ve složce `results/` 
