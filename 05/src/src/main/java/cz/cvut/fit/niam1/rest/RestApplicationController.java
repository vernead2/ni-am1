package cz.cvut.fit.niam1.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.xml.bind.DatatypeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


@RestController
public class RestApplicationController {

    @Autowired
    RestApplicationRepository repository;

    @PostMapping(value = "/tour/{id}/customer")
    @ResponseStatus(HttpStatus.OK)
    public void addCustomer(@PathVariable String id, @RequestBody String customer) {
        repository.addCustomer(id, customer);
    }

    @GetMapping("/tours/last-modified")
    public ResponseEntity<List<Tour>> getToursLastModified(WebRequest request) {
        if (request.checkNotModified(repository.lastModified)) {
            return null;
        }
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.maxAge(200, TimeUnit.SECONDS))
                .lastModified(repository.lastModified)
                .body(repository.getTours());
    }



    @GetMapping("/tours/weak-tag")
    public ResponseEntity<List<Tour>> getToursWeakTag(WebRequest request, @RequestHeader(value = "If-None-Match", required = false) String ifNoneMatch){
        List<Tour> tours = repository.getTours();

        // create weakTag by joining all weakTags of tours separated by comma
        String weakTag = String.valueOf(Objects.hash(tours.stream().map(Tour::getWeakTag).toArray()));
        weakTag = "W/\"" + weakTag + "\"";

        System.out.println("ifNoneMatch = " + ifNoneMatch);
        System.out.println("weakTag = " + weakTag);

        if (request.checkNotModified(repository.lastModified)) {
            return null;
        }
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.maxAge(200, TimeUnit.SECONDS))
                .eTag(weakTag)
                .body(repository.getTours());
    }

    @GetMapping("/tours/strong-tag")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Tour>> getToursStrongTag(WebRequest request, @RequestHeader(value = "If-None-Match", required = false) String ifNoneMatch) throws NoSuchAlgorithmException, JsonProcessingException {
        List<Tour> tours = repository.getTours();

        // serialize tours into json using jackson
        String toursString = new ObjectMapper().writeValueAsString(tours);
        byte[] digest = MessageDigest.getInstance("MD5").digest(toursString.getBytes());
        String StrongTag = DatatypeConverter.printHexBinary(digest);

        if (request.checkNotModified(StrongTag)) {
            return null;
        }
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.maxAge(200, TimeUnit.SECONDS))
                .eTag(StrongTag)
                .body(tours);
    }
}
