package cz.cvut.fit.niam1.rest;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

public class Tour {

    @JsonView
    String id;
    @JsonView
    String name;
    @JsonView
    List<String> customers;


    public Tour(String id, String name, List<String> customers) {
        this.id = id;
        this.name = name;
        this.customers = customers;
    }

    @JsonIgnore
    public String getWeakTag() {
        return String.valueOf(Objects.hash(id, name));
    }

    public void addCustomer(String customer) {
        customers.add(customer);
    }

}
