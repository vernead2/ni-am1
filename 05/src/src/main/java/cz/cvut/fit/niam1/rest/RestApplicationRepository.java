package cz.cvut.fit.niam1.rest;


import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class RestApplicationRepository {

    private static final List<Tour> tours = new ArrayList<>();

    long lastModified;

    @PostConstruct
    public void initRepo() {
        tours.add(new Tour("1", "Beach tour", new ArrayList<>()));
        tours.add(new Tour("2", "Ski tour", new ArrayList<>()));
        tours.add(new Tour("3", "City tour", new ArrayList<>()));
        this.setLastModified();
    }

    public void setLastModified() {
        lastModified = System.currentTimeMillis();
    }

    public List<Tour> getTours() {
        return tours;
    }

    public Tour getTourById(String id) {
        return tours.stream().filter(c -> c.id.equals(id)).findAny().orElse(null);
    }

    public void addCustomer(String id, String customer) {
        Tour tour = getTourById(id);
        tour.addCustomer(customer);
        this.setLastModified();
    }
}
